Spark installation info
------------------------
Spark will be installed at the location '/opt/spark-1.4.1' and its configuration files will be at the location '/opt/spark-1.4.1/conf'.

JDK installation info
----------------------
The JDK version installed will be JDK 8 u51. Java home will be at location '/opt/jdk1.8.0_51'.

Scala installation info
-----------------------
Scala v2.10.5 will be installaed at the location /opt/scala-2.10.5 and its executables will be placed in /usr/bin.

SBT installation info
----------------------
SBT v0.13.8 will be installed at the location /opt/sbt and its executables will be placed in /usr/bin.

SBT "Create Project Script"
----------------------------
"createSbtProject" is a helper script which creates SBT project structure and creates a assembly file for building a fat jar. This script file is placed in /usr/bin and can be used as follows.

	$ createSbtProject <PROJECT_DIR_NAME> <MAIN_SCALA_CLASSNAME>

Example:

	$ createSbtProject MyProject MyMain

This will create the following directory structure:

	MyProject/
	MyProject/project
	MyProject/project/assembly.sbt
	MyProject/build.sbt
	MyProject/target
	MyProject/src
	MyProject/src/test
	MyProject/src/test/java
	MyProject/src/test/resources
	MyProject/src/test/scala
	MyProject/src/main
	MyProject/src/main/java
	MyProject/src/main/resources
	MyProject/src/main/scala
	MyProject/src/main/scala/MyMain.scala
	MyProject/lib

To build a far jar, execute the following command from the root of the project directory:

	$ sbt assembly
	
To submit a Spark job
----------------------
    $ /opt/spark-1.4.1/bin/./spark-submit --class <MAIN_CLASS> \
        --master local[*] \
        --driver-memory 2g \
        --executor-memory 1g \
        <PATH_TO_FAT_JAR_FILE> \
